package com.luigui.demo.bootstrap;

import com.luigui.demo.domain.Author;
import com.luigui.demo.domain.Book;
import com.luigui.demo.domain.Publisher;
import com.luigui.demo.repositories.AuthorRepository;
import com.luigui.demo.repositories.BookRepository;
import com.luigui.demo.repositories.PublisherRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class BoostrapData implements CommandLineRunner {

    private final AuthorRepository authorRepository;
    private final BookRepository bookRepository;
    private final PublisherRepository publisherRepository;

    public BoostrapData(AuthorRepository authorRepository, BookRepository bookRepository, PublisherRepository publisherRepository) {
        this.authorRepository = authorRepository;
        this.bookRepository = bookRepository;
        this.publisherRepository = publisherRepository;
    }

    @Override
    public void run(String... args) throws Exception {

        Author luis = new Author("Luis","Ogando");
        Book cleaCode = new Book("Clean Code","12232");
        Publisher saint = new Publisher();
        luis.getBooks().add(cleaCode);
        cleaCode.getAuthors().add(luis);
        saint.setAddressLine1("c:regina,/.......");
        saint.setCity("santo domingo");
        saint.setName("Saint");
        saint.setState("DR");

        authorRepository.save(luis);
        bookRepository.save(cleaCode);
        publisherRepository.save(saint);

        System.out.println("Start......");
        System.out.println(authorRepository.count());
        System.out.println(bookRepository.count());
        System.out.println(publisherRepository.count());

    }
}
